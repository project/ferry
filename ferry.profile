<?php


/**
 * Implements hook_form_FORM_ID_alter().
 */
function ferry_form_install_configure_form_alter(&$form, $form_state) {
  $form['server_settings']['site_default_country']['#default_value'] = 'BE';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'Europe/Brussels';
}


/**
 * Correct translation_overview when Drupal is installed in another language
 * @see http://drupal.org/node/1800804
 */
function ferry_tov_correct() {
  global $install_state;

  if (isset($install_state['parameters']['locale'])) {
    $locale = $install_state['parameters']['locale'];

    if ($locale != 'en') {
      $load = drupal_load('module', 'translation_overview');
      $param = array(
        'values' => array(
          'langcode' => $locale,
        ),
      );
      translation_overview_schema_add_submit(NULL, $param);
    }
  }
  return NULL;
}


/*
 * Implements hook_install_tasks().
 */
function ferry_install_tasks($install_state) {
  $tasks = array(
    'ferry_tov_correct' => array(
      'display_name' => st('Translation Overview correction'),
      'display' => FALSE,
      'type' => 'normal',
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'function' => 'ferry_tov_correct',
    ),
  );

  return $tasks;
}
