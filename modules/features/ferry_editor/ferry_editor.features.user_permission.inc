<?php
/**
 * @file
 * ferry_editor.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ferry_editor_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer ckeditor.
  $permissions['administer ckeditor'] = array(
    'name' => 'administer ckeditor',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'ckeditor',
  );

  // Exported permission: customize ckeditor.
  $permissions['customize ckeditor'] = array(
    'name' => 'customize ckeditor',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'ckeditor',
  );

  return $permissions;
}
