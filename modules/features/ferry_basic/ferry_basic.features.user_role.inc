<?php
/**
 * @file
 * ferry_basic.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function ferry_basic_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => '2',
  );

  // Exported role: editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => '3',
  );

  // Exported role: editor-in-chief.
  $roles['editor-in-chief'] = array(
    'name' => 'editor-in-chief',
    'weight' => '4',
  );

  return $roles;
}
