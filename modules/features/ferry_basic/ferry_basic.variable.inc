<?php
/**
 * @file
 * ferry_basic.variable.inc
 */

/**
 * Implements hook_variable_realm_default_variables().
 */
function ferry_basic_variable_realm_default_variables() {
  $realm_variables = array();

  $realm_variables['global']['default'] = array(
    'block_cache' => 1,
    'cache' => 1,
    'clean_url' => '1',
    'comment_anonymous_article' => 0,
    'comment_anonymous_page' => 0,
    'comment_article' => '1',
    'comment_default_mode_article' => 1,
    'comment_default_mode_page' => 1,
    'comment_default_per_page_article' => '50',
    'comment_default_per_page_page' => '50',
    'comment_form_location_article' => 1,
    'comment_form_location_page' => 1,
    'comment_page' => '1',
    'comment_preview_article' => '1',
    'comment_preview_page' => '1',
    'comment_subject_field_article' => 1,
    'comment_subject_field_page' => 1,
    'date_api_version' => '7.2',
    'date_default_timezone' => 'Europe/Paris',
    'date_format_html5_tools_iso8601' => 'c',
    'enable_revisions_page_article' => 1,
    'enable_revisions_page_page' => 1,
    'field_bundle_settings_file__audio' => array(
      'view_modes' => array(),
      'extra_fields' => array(
        'form' => array(),
        'display' => array(
          'file' => array(
            'media_small' => array(
              'weight' => 0,
              'visible' => FALSE,
            ),
          ),
        ),
      ),
    ),
    'field_bundle_settings_file__default' => array(
      'view_modes' => array(),
      'extra_fields' => array(
        'form' => array(),
        'display' => array(
        'file' => array(
            'media_small' => array(
              'weight' => 0,
              'visible' => FALSE,
            ),
          ),
        ),
      ),
    ),
    'field_bundle_settings_file__image' => array(
      'view_modes' => array(),
      'extra_fields' => array(
        'form' => array(),
        'display' => array(
          'file' => array(
            'media_small' => array(
              'weight' => 0,
              'visible' => FALSE,
            ),
          ),
        ),
      ),
    ),
    'field_bundle_settings_file__video' => array(
      'view_modes' => array(),
      'extra_fields' => array(
      'form' => array(),
        'display' => array(
          'file' => array(
            'media_small' => array(
              'weight' => 0,
              'visible' => FALSE,
            ),
          ),
        ),
      ),
    ),
    'file_temporary_path' => '/tmp',
    'filter_fallback_format' => 'plain_text',
    'imce_profiles' => array(
      1 => array(
        'name' => 'User-1',
        'usertab' => 1,
        'filesize' => 0,
        'quota' => 0,
        'tuquota' => 0,
        'extensions' => '*',
        'dimensions' => '1200x1200',
        'filenum' => 0,
        'directories' => array(
          0 => array(
            'name' => '.',
            'subnav' => 1,
            'browse' => 1,
            'upload' => 1,
            'thumb' => 1,
            'delete' => 1,
            'resize' => 1,
          ),
        ),
        'thumbnails' => array(
          0 => array(
            'name' => 'Small',
            'dimensions' => '90x90',
            'prefix' => 'small_',
            'suffix' => '',
          ),
          1 => array(
            'name' => 'Medium',
            'dimensions' => '120x120',
            'prefix' => 'medium_',
            'suffix' => '',
          ),
          2 => array(
            'name' => 'Large',
            'dimensions' => '180x180',
            'prefix' => 'large_',
            'suffix' => '',
          ),
        ),
      ),
      2 => array(
        'name' => 'Sample profile',
        'usertab' => 1,
        'filesize' => 1,
        'quota' => 2,
        'tuquota' => 0,
        'extensions' => 'gif png jpg jpeg',
        'dimensions' => '800x600',
        'filenum' => 1,
        'directories' => array(
          0 => array(
            'name' => 'u%uid',
            'subnav' => 0,
            'browse' => 1,
            'upload' => 1,
            'thumb' => 1,
            'delete' => 0,
            'resize' => 0,
          ),
        ),
        'thumbnails' => array(
          0 => array(
            'name' => 'Thumb',
            'dimensions' => '90x90',
            'prefix' => 'thumb_',
            'suffix' => '',
          ),
        ),
      ),
    ),
    'node_options_article' => array(
      0 => 'status',
      1 => 'promote',
    ),
    'node_options_page' => array(
      0 => 'status',
    ),
    'node_preview_article' => '1',
    'node_preview_page' => '1',
    'node_submitted_article' => 0,
    'node_submitted_page' => 0,
    'pathauto_blog_pattern' => 'blogs/[user:name]',
    'pathauto_forum_pattern' => '[term:vocabulary]/[term:name]',
    'pathauto_node_pattern' => 'content/[node:title]',
    'pathauto_punctuation_hyphen' => 1,
    'pathauto_taxonomy_term_pattern' => '[term:vocabulary]/[term:name]',
    'pathauto_user_pattern' => 'users/[user:name]',
    'path_alias_whitelist' => array(
      'user' => TRUE,
    ),
    'user_pictures' => 1,
    'user_picture_dimensions' => '1024x1024',
    'user_picture_file_size' => 512,
    'user_picture_style' => 'thumbnail',
    'user_register' => 2,
    'xmlsitemap_settings_node_article' => array(
      'status' => '1',
      'priority' => '0.5',
    ),
    'xmlsitemap_settings_node_page' => array(
      'status' => '1',
      'priority' => '0.5',
    ),
  );

  return $realm_variables;
}
