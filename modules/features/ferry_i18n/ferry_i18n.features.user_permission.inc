<?php
/**
 * @file
 * ferry_i18n.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ferry_i18n_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer content translations.
  $permissions['administer content translations'] = array(
    'name' => 'administer content translations',
    'roles' => array(
      'administrator' => 'administrator',
      'editor-in-chief' => 'editor-in-chief',
    ),
    'module' => 'i18n_node',
  );

  // Exported permission: administer languages.
  $permissions['administer languages'] = array(
    'name' => 'administer languages',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'locale',
  );

  // Exported permission: manage en translation overview priorities.
  $permissions['manage en translation overview priorities'] = array(
    'name' => 'manage en translation overview priorities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'translation_overview',
  );

  // Exported permission: translate admin strings.
  $permissions['translate admin strings'] = array(
    'name' => 'translate admin strings',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'i18n_string',
  );

  // Exported permission: translate content.
  $permissions['translate content'] = array(
    'name' => 'translate content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor-in-chief' => 'editor-in-chief',
    ),
    'module' => 'translation',
  );

  // Exported permission: translate interface.
  $permissions['translate interface'] = array(
    'name' => 'translate interface',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor-in-chief' => 'editor-in-chief',
    ),
    'module' => 'locale',
  );

  // Exported permission: translate user-defined strings.
  $permissions['translate user-defined strings'] = array(
    'name' => 'translate user-defined strings',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'editor-in-chief' => 'editor-in-chief',
    ),
    'module' => 'i18n_string',
  );

  // Exported permission: view translation overview assigments.
  $permissions['view translation overview assigments'] = array(
    'name' => 'view translation overview assigments',
    'roles' => array(
      'administrator' => 'administrator',
      'editor-in-chief' => 'editor-in-chief',
    ),
    'module' => 'translation_overview',
  );

  return $permissions;
}
