<?php
/**
 * @file
 * ferry_18n.variable.inc
 */

/**
 * Implements hook_variable_realm_default_variables().
 */
function ferry_i18n_variable_realm_default_variables() {
  $realm_variables = array();

  $realm_variables['global']['default'] = array(
    'i18n_node_extended_article' => '1',
    'i18n_node_extended_page' => '1',
    'i18n_node_options_article' => array(
      0 => 'required',
    ),
    'i18n_node_options_page' => array(
      0 => 'required',
    ),
    'i18n_sync_node_type_article' => array(
      0 => 'comment',
      1 => 'status',
      2 => 'promote',
      3 => 'moderate',
      4 => 'sticky',
    ),
    'i18n_sync_node_type_page' => array(
      0 => 'comment',
      1 => 'status',
      2 => 'promote',
      3 => 'moderate',
      4 => 'sticky',
    ),
    'language_content_type_article' => '2',
    'language_content_type_page' => '2',
    'language_negotiation_language' => array(),
    'language_negotiation_language_content' => array(
      'locale-interface' => array(
        'callbacks' => array(
          'language' => 'locale_language_from_interface',
        ),
        'file' => 'includes/locale.inc',
      ),
    ),
    'language_negotiation_language_url' => array(
      'locale-url' => array(
        'callbacks' => array(
          'language' => 'locale_language_from_url',
          'switcher' => 'locale_language_switcher_url',
          'url_rewrite' => 'locale_language_url_rewrite_url',
        ),
        'file' => 'includes/locale.inc',
      ),
      'locale-url-fallback' => array(
        'callbacks' => array(
          'language' => 'locale_language_url_fallback',
        ),
        'file' => 'includes/locale.inc',
      ),
    ),
    'language_types' => array(
      'language' => TRUE,
      'language_content' => FALSE,
      'language_url' => FALSE,
    ),
  );

  return $realm_variables;
}
