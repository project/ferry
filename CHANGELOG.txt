7.x-1.3
-------
Issue #2373227: upgraded core, libraries and modules

7.x-1.2
-------
Issue #2233697: upgraded print
Issue #2228459: upgraded registration
Issue #2228429: upgraded recaptcha
Issue #2228425: upgraded panels
Issue #2228423: upgraded og
Issue #2228417: upgraded l10n_update
Issue #2226737: upgraded colorbox library
Issue #2226731: upgraded views_slideshow
Issue #2226725: upgraded adaptivetheme
Issue #2226719: upgraded sky
Issue #2226715: upgraded site_map
Issue #2226709: upgraded webform
Issue #2226707: upgraded webform_validation

7.x-1.1
-------
Issue #2182299: removed link patch
Issue #2183783: upgraded superfish library
Issue #2183773: upgraded search_api_solr
Issue #2183769: upgraded search_api_db
Issue #2183763: upgraded search_api

7.x-1.1-beta3
-------------
Issue #2182947: upgraded apachesolr_multilingual
Issue #2182935: upgraded apachesolr_confgen
Issue #2182931: upgraded apachesolr
Issue #2182929: upgraded views_data_export
Issue #2182843: upgraded performance

7.x-1.1-beta2
-------------
Issue #2182833: upgraded plupload
Issue #2182831: upgraded geofield
Issue #2182829: upgraded colorbox
Issue #2182819: upgraded views_bulk_operations
Issue #2182307: upgraded seckit
Issue #2182303: upgraded options_element
Issue #2182299: upgraded link
Issue #2182297: upgraded imce
Issue #2182293: upgraded addtoany

7.x-1.1-beta1
-------------
Issue #2182087: upgraded plupload library
Issue #2181825: upgraded colorbox library
Issue #2181813: upgraded honeypot
Issue #2181781: upgraded variable
Issue #2181779: upgraded date
Issue #2181769: upgraded og
Issue #2181765: upgraded entityreference
Issue #2181761: upgraded drupal core
Issue #2181755: upgraded addressfield
Issue #2181751: upgraded entity
Issue #2181747: upgraded ckeditor library

7.x-1.0
-------
Issue #2117005: upgraded features
Issue #2117011: upgraded ckeditor library
Issue #2117013: upgraded colorbox library
Issue #2117019: upgraded security_review
Issue #2117023: upgraded field_group
Issue #2117027: upgraded plupload
Issue #2117031: upgraded google_analytics
Issue #2126083: upgraded rules
Issue #2126087: upgraded file_entity
Issue #2126089: upgraded media
Issue #2126093: upgraded search_api
Issue #2126095: upgraded apachesolr
Issue #2126101: upgraded quiz
Issue #2126105: upgraded search_api_db
Issue #2126109: upgraded search_api_solr
Issue #2128343: added honeypot

7.x-1.0-rc3
-----------
Issue #2104283: upgraded search_api_db
Issue #2104279: upgraded features
Issue #2096297: upgraded addtoany
Issue #2093667: upgraded rules
Issue #2096287: upgraded geofield
Issue #2093669: upgraded seckit
Issue #2088247: upgraded ds

7.x-1.0-rc2
-----------
Issue #2079027: upgraded l10n_client
Issue #2079023: upgraded search_api_solr
Issue #2078901: upgraded features
Issue #2078903: upgraded field_group
Issue #2078905: upgraded file_entity
Issue #2078907: upgraded i18n
Issue #2078973: upgraded webform_validation
Issue #2078977: upgraded recaptcha
Issue #2078983: upgraded search_api
Issue #2089909: upgraded plupload
Issue #2078971: upgraded ds
Issue #2067823: upgraded entity
Issue #2067821: upgraded variable
Issue #2067787: upgraded registration

7.x-1.0-rc1
-----------
Issue #2060643: upgraded extlink
Issue #2060641: upgraded apachesolr
Issue #2060639: upgraded apachesolr_multilingual
Issue #2060637: upgraded apachesolr_attachments
Issue #2060605: upgraded entity_translation
Issue #2060551: upgraded geofield
Issue #2060537: upgraded features
Issue #2060525: upgraded addtoany
Issue #2060521: upgraded seckit
Issue #2060515: upgraded og
Issue #2060503: upgraded drupal core

7.x-1.0-beta2
-------------
Issue #2048641: upgraded search_api_db
Issue #2048639: upgraded search_api_solr
Issue #2048635: upgraded addtoany
Issue #2048631: upgraded ckeditor
Issue #2048627: upgraded message
Issue #2019763: upgraded twitter_block
Issue #2039053: upgraded colorbox library
Issue #2039049: upgraded media
Issue #2039045: upgraded plupload
Issue #2039043: upgraded search_api
Issue #2039041: upgraded i18n
Issue #2039037: upgraded elements
Issue #2039031: upgraded captcha

7.x-1.0-beta1
-------------
Issue #2019757: upgraded ckeditor library
Issue #2019701: upgraded ds
Issue #2016971: upgraded search_api
Issue #2016985: upgraded search_api_solr
Issue #2016963: upgraded apachesolr
Issue #2014759: upgraded colorbox library
Issue #2012060: upgraded features
Issue #1999094: upgraded admin_views
Issue #2012062: upgraded webform
Issue #2001524: added views_data_export
Issue #1696848: added geofield
Issue #1696848: added geocoder
Issue #1696848: added geophp library
Issue #1992250: added simplepie library again

7.x-1.0-alpha4
--------------
Issue #1992250: removed simplepie library (for now)
Issue #1998588: fixed notice in quiz
Issue #1998572: upgraded feeds_xpathparser
Issue #1998566: upgraded search_api_solr
Issue #1998556: upgraded search_api_db
Issue #1997270: added calendar
Issue #1992250: added simplepie library
Issue #1996566: upgraded ds
Issue #1994754: upgraded colorbox
Issue #1994752: upgraded plupload
Issue #1992218: upgraded apachesolr_confgen
Issue #1992214: upgraded addressfield
Issue #1992190: upgraded search_api
Issue #1992204: upgraded apachesolr_multilingual

7.x-1.0-alpha3
--------------
Issue #1984482: upgraded entity
Issue #1984476: upgraded ckeditor library
Issue #1984470: added feeds_xpathparser
Issue #1981878: upgraded options_element
Issue #1981874: upgraded superfish
Issue #1979906: upgraded feeds
Issue #1977038: upgraded search_api_solr
Issue #1977032: upgraded message_notify 
Issue #1977036: upgraded og
Issue #1977030: upgraded message
Issue #1975038: upgraded colorbox library
Issue #1975034: upgraded colorbox
Issue #1970164: upgraded ckeditor
Issue #1967890: upgraded apachesolr

7.x-1.0-alpha2
--------------
Issue #1966236: fixed build file
Issue #1959642: upgraded colorbox library
Issue #1965680: upgraded views
Issue #1965682: upgraded to Drupal core 7.22
Issue #1960622: upgraded ctools

7.x-1.0-alpha1
--------------
Issue #1959180: upgraded facetapi
Issue #1956920: upgraded features
Issue #1952452: upgraded ckeditor library
Issue #1954092: upgraded og
Issue #1954508: upgraded rules
Issue #1949312: upgraded title
Issue #1949310: upgraded views
Issue #1944802: upgraded elements
Issue #1944800: upgraded webform_validation

7.x-0.13
--------
Issue #1940686: added link patch
Issue #1940452: updated arrays in ..user_permission.inc
Issue #1936360: added feature admin
Issue #1939032: upgraded ckeditor library to full version
Issue #1937606: fixed feature editor overrides 
Issue #1939478: upgraded message notify
Issue #1939476: deleted plupload example directory
Issue #1937606: removed plugins section from feature editor
Issue #1937606: removed ckeditor from feature basic dependencies
Issue #1936360: enabled admin_menu_toolbar by default
Issue #1936360: fixed typo in admin_menu dependency
Issue #1939296: upgraded libraries
Issue #1939032: upgraded ckeditor library
Issue #1938896: upgraded linkit
Issue #1938412: changed basic feature
Issue #1937606: fixed ckeditor paths
Issue #1936360: added admin_menu
Issue #1936658: upgraded to Drupal core 7.21

7.x-0.12
--------
Issue #1934362: upgraded features
Issue #1934242: upgraded registration
Issue #1904090: added and upgraded plupload library
Issue #1934244: upgraded ckeditor library
Issue #1934238: upgraded quiz
Issue #1926300: upgraded token
Issue #1926286: upgraded to Drupal core 7.20
Issue #1926272: upgraded ds
Issue #1926264: upgraded l10n_client
Issue #1921630: upgraded linkchecker
Issue #1921584: upgraded securelogin
Issue #1913062: upgraded link
Issue #1904090: remove plupload library (for now)
Issue #1920968: upgraded variable
Issue #1920966: upgraded colorbox
Issue #1920962: upgraded jquery_update
Issue #1920960: upgraded recaptcha
Issue #1920950: upgraded og

7.x-0.11
--------
Issue #1904090: downgraded plupload library

7.x-0.10
--------
Issue #1904090: upgraded plupload library
Issue #1904086: upgraded plupload
Issue #1904082: upgraded imce 
Issue #1904078: upgraded registration
Issue #1894820: upgraded search_api_solr
Issue #1894564: upgraded performance
Issue #1894556: upgraded ckeditor library
Issue #1894552: upgraded og
Issue #1894550: upgraded search_api
Issue #1894548: upgraded seckit
Issue #1894546: upgraded i18n
Issue #1894538: upgraded to Drupal core 7.19

7.x-0.9
-------
Issue #1875082: upgraded entity
Issue #1875074: upgraded to Drupal core 7.18
Issue #1872018: upgraded xmlsitemap
Issue #1871412: upgraded title
Issue #1871408: upgraded emfield
Issue #1871404: upgraded og
Issue #1869540: upgraded views_bulk_operations
Issue #1869534: upgraded message to 1.7
Issue #1868884: upgraded ckeditor
Issue #1855740: upgraded quiz
Issue #1855742: upgraded entity_translation
Issue #1855738: upgraded diff
Issue #1844176: upgraded file_entity
Issue #1844170: upgraded entityreference
Issue #1844166: upgraded og
Issue #1844164: upgraded media
Issue #1844154: upgraded message_notify
Issue #1843548: upgraded entity_translation
Issue #1843546: upgraded talk
Issue #1834772: upgraded message
Issue #1834774: upgraded search_api_db
Issue #1837172: upgraded imce
Issue #1837176: upgraded to Drupal core 7.17
Issue #1830418: upgraded quiz
Issue #1830422: upgraded google_analytics
Issue #1823854: upgraded apachesolr_og
Issue #1823856: upgraded feeds
Issue #1823852: upgraded to Drupal core 7.16

Issue #1823854: upgraded apachesolr_og
Issue #1823856: upgraded feeds
Issue #1823852: upgraded to Drupal core 7.16


7.x-0.8
-------
Issue #1818612: upgraded quiz
Issue #1818606: upgraded colorbox
Issue #1818604: upgraded search_api
Issue #1811940: upgraded apachesolr
Issue #1809164: upgraded feeds
Issue #1808432: upgraded ckeditor library
Issue #1807734: upgraded recaptcha
Issue #1804450: XAMPP: moved rdf in .info file + disabled seckit
Issue #1805394: upgraded html5_media
Issue #1805392: upgraded ds
Issue #1790292: features info files cleanup
Issue #1800804: added translation_overview install task 
Issue #1803790: moved i18n from ferry_basic Feature to ferry_i18n
Issue #1803112: disabled i18n_redirect
Issue #1800496: upgraded apachesolr

7.x-0.7
-------
Issue #1793588: upgraded entityreference
Issue #1793584: upgraded token
Issue #1792408: upgraded ds
Issue #1790732: upgraded facetapi
Issue #1790302: added apachesolr_og
Issue #1790292: features info files cleanup
Issue #1790254: added og feature
Issue #1789342: upgraded elements
Issue #1788334: upgraded apachesolr
Issue #1788296: upgraded og
Issue #1788074: upgraded apachesolr_multilingual
Issue #1787082: enabled seckit
Issue #1786938: disabled apachesolr_confgen
Issue #1785218: upgraded views_bulk_operations

7.x-0.6
-------
Issue #1783876: upgraded options_element
Issue #1782808: upgraded token
Issue #1781426: upgraded querypath
Issue #1779536: upgraded registration_date
Issue #1779534: upgraded seckit
Issue #1777736: added og_extras
Issue #1774938: removed solrphpclient library
Issue #1773788: added feeds and job_scheduler
Issue #1773872: upgraded print
Issue #1772970: upgraded apachesolr_multilingual
Issue #1772564: added devel Feature
Issue #1772452: upgraded apachesolr_confgen

7.x-0.5
-------

Issue #1757850: changed colorbox library url
Issue #1757400: upgraded ds to 2.0-beta3
Issue #1765120: upgraded message_notify
Issue #1770624: updated version in .info
Issue #1765180: moved i18nviews to basic Feature
Issue #1765176: fixed colorbox installation
Issue #1765156: upgraded adaptivetheme and sky themes
Issue #1765120: added message and message_notify
Issue #1764982: upgraded apachesolr_autocomplete
Issue #1764980: upgraded variable
Issue #1764978: added seckit
Issue #1763410: upgraded email
Issue #1760232: added menu_block
Issue #1760170: added globalredirect
Issue #1750762: upgraded linkchecker
Issue #1757852: upgraded adaptivetheme
Issue #1757850: upgraded colorbox library
Issue #1757840: upgraded title
Issue #1757396: upgraded rules
Issue #1757400: upgraded ds
Issue #1757546: upgraded panels
Issue #1757390: upgraded views
Issue #1721910: upgraded ctools to 1.2
Issue #1729822: upgraded date
Issue #1729824: upgraded token
Issue #1727810: upgraded apachesolr
Issue #1727814: upgraded apachesolr_confgen
Issue #1721918: upgraded pathauto
Issue #1721910: upgraded ctools

7.x-0.4
-------
Issue #1702312: added querypath
Issue #1702312: changed solr Feature version 
Issue #1702312: added apachesolr_confgen
Issue #1711686: added experimental Feature
Issue #1710360: commented out registration and registration_date
Issue #1710360: added registration and registration_date
Issue #1710046: upgraded facetapi
Issue #1710042: upgraded to Drupal core 7.15
Issue #1702338: removed beididp
Issue #1702312: enabled apachesolr_multilingual
Issue #1702312: added apachesolr_multilingual
Issue #1701346: upgraded ckeditor library
Issue #1701314: upgraded secure login
Issue #1701308: upgraded features

7.x-0.3
-------
Issue #1692354: added ckeditor permissions
Issue #1698168: used variable_realm for options
Issue #1697704: disabled search_api_db
Issue #1697608: added webform Feature
Issue #1697600: disabled views slideshow
Issue #1696930: added FAQ
Issue #1696926: added address field
Issue #1696866: fixed HTTP 500 error
Issue #1691024: removed entity_translation
Issue #1694578: removed entitycache
Issue #1693560: added quiz
Issue #1694482: removed fivestar and voting api
Issue #1692354: improved ckeditor Feature
Issue #1692324: moved themes ot profile/ferry/themes
Issue #1692296: fixed permissions and dependencies in Basic

7.x-0.2
-------
Issue #1690794: fixed upgrading theme to AT 3 and Sky 3
Issue #1691360: added profile version to build file
Issue #1691024: fixed disabling entity translation
Issue #1691062: moved module settings to basic Feature set
Issue #1691024: disabled entity translation
Issue #1690794: upgraded theme to AT 3 and Sky 3
Issue #1690782: removed webform permissions from Feature
Issue #1690778: fixed creating specific roles
Issue #1689418: added views_bulk_operations
Issue #1689416: added entityreference
