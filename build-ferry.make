api = 2
core = 7.x

projects[drupal][version] = "7.33"

projects[ferry][type] = profile
projects[ferry][version] = 1.x-dev
projects[ferry][download][type] = git
projects[ferry][download][branch] = 7.x-1.x
