api = 2
core = 7.x
;
; Libraries
;

; CK Editor
libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.2.3/ckeditor_4.2.3_full.tar.gz

; GeoPHP
libraries[geophp][download][type] = get
libraries[geophp][download][url] = https://github.com/downloads/phayes/geoPHP/geoPHP.tar.gz 

; Plupload
libraries[plupload][download][type] = get
libraries[plupload][download][url] = https://github.com/moxiecode/plupload/archive/v1.5.8.zip 
libraries[plupload][patch][1903850] = https://drupal.org/files/issues/plupload-1_5_8-rm_examples-1903850-16.patch

; Simplepie
libraries[simplepie][download][type] = get 
libraries[simplepie][download][url] = http://simplepie.org/downloads/simplepie_1.3.1.mini.php

; Superfish
libraries[superfish][download][type] = get
libraries[superfish][download][url] = https://github.com/mehrpadin/Superfish-for-Drupal/tarball/bb9c24a3db1aeebf832d67b54a23b74cb5e982c4

; ColorBox
libraries[colorbox][download][type] = get
libraries[colorbox][download][url] = https://github.com/jackmoore/colorbox/archive/1.5.14.zip 

;
; Themes
;
projects[adaptivetheme][version] = 3.2
projects[sky][version] = 3.0

;
; Modules
;

; Development
projects[devel][version] = 1.5
projects[devel][subdir] = contrib
projects[drupal_reset][version] = 1.4
projects[drupal_reset][subdir] = contrib
projects[features][version] = 2.2
projects[features][subdir] = contrib
projects[performance][version] = 2.0
projects[performance][subdir] = contrib

; Experimental
projects[entity_translation][version] = 1.0-beta3
projects[entity_translation][subdir] = contrib
projects[i18nviews][version] = 3.x-dev
projects[i18nviews][subdir] = contrib 
projects[registration][version] = 1.3
projects[registration][subdir] = contrib
projects[registration_date][version] = 1.0-alpha2
projects[registration_date][subdir] = contrib
projects[search_api_et][version] = 1.x-dev
projects[search_api_et][subdir] = contrib 

; Fields
projects[addressfield][version] = 1.0-beta5
projects[addressfield][subdir] = contrib
projects[date][version] = 2.8
projects[date][subdir] = contrib
projects[email][version] = 1.3
projects[email][subdir] = contrib
projects[emfield][version] = 1.0-alpha2
projects[emfield][subdir] = contrib
projects[field_group][version] = 1.4
projects[field_group][subdir] = contrib
projects[link][version] = 1.3
projects[link][subdir] = contrib
projects[media][version] = 2.0-alpha4
projects[media][subdir] = contrib

; Forms
projects[multiform][version] = 1.1
projects[multiform][subdir] = contrib
projects[webform][version] = 3.21
projects[webform][subdir] = contrib
projects[webform_validation][version] = 1.7
projects[webform_validation][subdir] = contrib

; Framework
projects[ctools][version] = 1.4
projects[ctools][subdir] = contrib
projects[elements][version] = 1.4
projects[elements][subdir] = contrib
projects[entity][version] = 1.5
projects[entity][subdir] = contrib
projects[entityreference][version] = 1.1
projects[entityreference][subdir] = contrib
projects[feeds][version] = 2.0-alpha8
projects[feeds][subdir] = contrib
projects[feeds_xpathparser][version] = 1.0-beta4
projects[feeds_xpathparser][subdir] = contrib
projects[file_entity][version] = 2.0-beta1
projects[file_entity][subdir] = contrib
projects[geocoder][version] = 1.2
projects[geocoder][subdir] = contrib
projects[geofield][version] = 2.3
projects[geofield][subdir] = contrib
projects[globalredirect][version] = 1.5
projects[globalredirect][subdir] = contrib
projects[html5_media][version] = 1.1
projects[html5_media][subdir] = contrib
projects[html5_tools][version] = 1.2
projects[html5_tools][subdir] = contrib
projects[job_scheduler][version] = 2.0-alpha3
projects[job_scheduler][subdir] = contrib
projects[libraries][version] = 2.2
projects[libraries][subdir] = contrib
projects[linkchecker][version] = 1.2
projects[linkchecker][subdir] = contrib
projects[pathauto][version] = 1.2
projects[pathauto][subdir] = contrib
projects[token][version] = 1.5
projects[token][subdir] = contrib
projects[variable][version] = 2.5
projects[variable][subdir] = contrib
projects[views_data_export][version] = 3.0-beta8
projects[views_data_export][subdir] = contrib

; Languages and localization
projects[facetapi_i18n][version] = 1.0-beta2
projects[facetapi_i18n][subdir] = contrib
projects[i18n][version] = 1.11
projects[i18n][subdir] = contrib
projects[l10n_client][version] = 1.3
projects[l10n_client][subdir] = contrib
projects[l10n_update][version] = 1.1
projects[l10n_update][subdir] = contrib
projects[lang_dropdown][version] = 1.5
projects[lang_dropdown][subdir] = contrib
projects[languageicons][version] = 1.1
projects[languageicons][subdir] = contrib
projects[title][version] = 1.0-alpha7
projects[title][subdir] = contrib
projects[translation_overview][version] = 2.0-beta1
projects[translation_overview][subdir] = contrib
projects[translation_table][version] = 1.0-beta1
projects[translation_table][subdir] = contrib
projects[transliteration][version] = 3.2
projects[transliteration][subdir] = contrib

; Marketing
projects[google_analytics][version] = 1.4
projects[google_analytics][subdir] = contrib

; Rules
projects[rules][version] = 2.7
projects[rules][subdir] = contrib

; Search
projects[apachesolr][version] = 1.7
projects[apachesolr][subdir] = contrib
projects[apachesolr_attachments][version] = 1.3
projects[apachesolr_attachments][subdir] = contrib
projects[apachesolr_autocomplete][version] = 1.4
projects[apachesolr_autocomplete][subdir] = contrib
projects[apachesolr_confgen][version] = 1.1
projects[apachesolr_confgen][subdir] = contrib 
projects[apachesolr_multilingual][version] = 1.2
projects[apachesolr_multilingual][subdir] = contrib
projects[apachesolr_og][version] = 1.0
projects[apachesolr_og][subdir] = contrib
projects[facetapi][version] = 1.5
projects[facetapi][subdir] = contrib
projects[querypath][version] = 2.1
projects[querypath][subdir] = contrib
projects[search_api][version] = 1.13
projects[search_api][subdir] = contrib
projects[search_api_db][version] = 1.4
projects[search_api_db][subdir] = contrib
projects[search_api_solr][version] = 1.6
projects[search_api_solr][subdir] = contrib

; Social and other media
projects[addtoany][version] = 4.6
projects[addtoany][subdir] = contrib
projects[faq][version] = 1.0-rc3
projects[faq][subdir] = contrib
projects[mail_edit][version] = 1.0
projects[mail_edit][subdir] = contrib
projects[message][version] = 1.9
projects[message][subdir] = contrib
projects[message_notify][version] = 2.5
projects[message_notify][subdir] = contrib
projects[print][version] = 2.0
projects[print][subdir] = contrib
projects[quiz][version] = 4.0-beta2
projects[quiz][subdir] = contrib
projects[twitter_block][version] = 2.2
projects[twitter_block][subdir] = contrib

; User interface
projects[admin_menu][version] = 3.0-rc4
projects[admin_menu][subdir] = contrib
projects[admin_views][version] = 1.3
projects[admin_views][subdir] = contrib
projects[calendar][version] = 3.5
projects[calendar][subdir] = contrib
projects[colorbox][version] = 2.8
projects[colorbox][subdir] = contrib
projects[ckeditor][version] = 1.16
projects[ckeditor][subdir] = contrib
projects[diff][version] = 3.2 
projects[diff][subdir] = contrib
projects[ds][version] = 2.7
projects[ds][subdir] = contrib
projects[extlink][version] = 1.18
projects[extlink][subdir] = contrib
projects[hansel][version] = 1.4
projects[hansel][subdir] = contrib
projects[imce][version] = 1.9
projects[imce][subdir] = contrib
projects[jquery_update][version] = 2.4
projects[jquery_update][subdir] = contrib
projects[linkit][version] = 2.6
projects[linkit][subdir] = contrib
projects[menu_block][version] = 2.4
projects[menu_block][subdir] = contrib
projects[modal_forms][version] = 1.2
projects[modal_forms][subdir] = contrib 
projects[options_element][version] = 1.12
projects[options_element][subdir] = contrib
projects[panels][version] = 3.4
projects[panels][subdir] = contrib
projects[plupload][version] = 1.7
projects[plupload][subdir] = contrib
projects[superfish][version] = 1.9
projects[superfish][subdir] = contrib
projects[talk][version] = 1.0
projects[talk][subdir] = contrib
projects[views][version] = 3.8
projects[views][subdir] = contrib
projects[views_slideshow][version] = 3.1
projects[views_slideshow][subdir] = contrib
projects[views_bulk_operations][version] = 3.2
projects[views_bulk_operations][subdir] = contrib

; Users and Security
projects[logintoboggan][version] = 1.4
projects[logintoboggan][subdir] = contrib
projects[og][version] = 2.7
projects[og][subdir] = contrib
projects[og_extras][version] = 1.1
projects[og_extras][subdir] = contrib
projects[role_delegation][version] = 1.1
projects[role_delegation][subdir] = contrib
projects[seckit][version] = 1.9
projects[seckit][subdir] = contrib 
projects[securelogin][version] = 1.4
projects[securelogin][subdir] = contrib
projects[security_review][version] = 1.2
projects[security_review][subdir] = contrib

; Spam protection
projects[captcha][version] = 1.1
projects[captcha][subdir] = contrib
projects[honeypot][version] = 1.17
projects[honeypot][subdir] = contrib
projects[recaptcha][version] = 1.11
projects[recaptcha][subdir] = contrib

; XML Sitemap and SEO
projects[page_title][version] = 2.7
projects[page_title][subdir] = contrib
projects[site_map][version] = 1.2
projects[site_map][subdir] = contrib
projects[site_verify][version] = 1.1
projects[site_verify][subdir] = contrib
projects[xmlsitemap][version] = 2.0
projects[xmlsitemap][subdir] = contrib
